@ECHO OFF
SET NODEJS_IS=FALSE
SET CALLBACK=%1

SET HKEY_QUERY_NODEJS=HKEY_LOCAL_MACHINE\SOFTWARE\Node.js

@REG QUERY "%HKEY_QUERY_NODEJS%" /v Version >>temp.txt
if %ERRORLEVEL% EQU  1 (

  SET NODEJS_IS=FALSE
  ECHO . Could not find

) ELSE (

  SET Cmd=REG QUERY "%HKEY_QUERY_NODEJS%" /s
  FOR /f "tokens=2*" %%i IN ('%Cmd% ^| find "Version"') DO (
    SET NodejsVersion=%%j
    SET NODEJS_IS=TRUE
  )

  ECHO NodejsVersion = %NodejsVersion% 

)

:end

::CALL RUN %CALLBACK% %NODEJS_IS%
